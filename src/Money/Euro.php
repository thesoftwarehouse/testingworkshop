<?php

namespace App\Money;

class Euro
{
    private $amount;

    public function __construct(float $amount)
    {
        $this->amount = $amount;
    }

    public function multiply(float $multiplier): self
    {
        return new Euro($this->amount*$multiplier);
    }

    public function equals(Euro $euro): bool
    {
        return $euro->amount === $this->amount;
    }
}
