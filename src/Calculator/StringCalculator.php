<?php
namespace App\Calculator;

use App\Calculator\Exception\InvalidInputStringException;

class StringCalculator
{
    public function add(string $numbers, string $separator): int
    {
        $parsedInput = $this->parse($numbers, $separator);

        $this->dataGuard($parsedInput, $numbers);

        return array_sum($parsedInput);
    }

    public function multiply(string $numbers, string $separator): int
    {
        $parsedInput = $this->parse($numbers, $separator);

        $this->dataGuard($parsedInput, $numbers);

        $result = 1;
        foreach ($parsedInput as $number) {
            $result *= $number;
        }

        return $result;
    }

    private function parse(string $numbers, string $separator): array
    {
        return explode($separator, $numbers);
    }

    private function dataGuard(array $input, string $rawInput): void
    {
        $notNumericItems = array_filter(
            $input,
            function ($item) {
                return !ctype_digit($item);
            }
        );

        if (\count($notNumericItems) > 0) {
            throw new InvalidInputStringException(
                sprintf(
                    "'%s' is not valid input",
                    $rawInput
                )
            );
        }
    }
}
