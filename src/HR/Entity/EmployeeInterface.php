<?php

namespace App\HR\Entity;

interface EmployeeInterface
{
    public function email(): string;
}
