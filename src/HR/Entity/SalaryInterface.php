<?php

namespace App\HR\Entity;

use App\Money\Euro;

interface SalaryInterface
{
    public function employeeId(): string;
    public function amount(): Euro;
    public function createdAt(): \DateTimeInterface;
}
