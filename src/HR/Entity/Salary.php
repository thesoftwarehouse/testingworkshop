<?php

namespace App\HR\Entity;

use App\Money\Euro;

class Salary implements SalaryInterface
{
    /** @var string */
    private $employeeId;

    /** @var Euro */
    private $amount;

    /** @var \DateTimeInterface */
    private $createdAt;

    public function __construct(string $employeeId, Euro $amount)
    {
        $this->employeeId = $employeeId;
        $this->amount = $amount;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function employeeId(): string
    {
        return $this->employeeId;
    }

    public function amount(): Euro
    {
        return $this->amount;
    }

    public function createdAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}
