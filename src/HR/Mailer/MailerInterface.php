<?php

namespace App\HR\Mailer;

interface MailerInterface
{
    public function send(string $email, string $message): void;
}
