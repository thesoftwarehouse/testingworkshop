<?php

namespace App\HR;

use App\HR\Entity\Salary;
use App\HR\Mailer\MailerInterface;
use App\HR\Repository\EmployeeRepositoryInterface;
use App\HR\Repository\SalaryRepositoryInterface;
use App\Money\Euro;

class SalaryService
{
    private const HR_EMAIL = 'hr@foo.bar';

    /** @var EmployeeRepositoryInterface */
    private $employeeRepository;

    /** @var SalaryRepositoryInterface */
    private $salaryRepository;

    /** @var MailerInterface */
    private $mailer;

    public function __construct(
        EmployeeRepositoryInterface $employeeRepository,
        SalaryRepositoryInterface $salaryRepository,
        MailerInterface $mailer
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->salaryRepository = $salaryRepository;
        $this->mailer = $mailer;
    }

    public function createPaycheck(string $employeeId, Euro $amount): void
    {
        $employee = $this->employeeRepository->find($employeeId);

        $salary = new Salary($employeeId, $amount);

        $this->salaryRepository->save($salary);

        $this->mailer->send($employee->email(), 'new salary');
        $this->mailer->send(self::HR_EMAIL, 'new salary');
    }
}
