<?php

namespace App\HR\Repository;

use App\HR\Entity\SalaryInterface;

interface SalaryRepositoryInterface
{
    public function save(SalaryInterface $salary): void;
}
