<?php

namespace App\HR\Repository;

use App\HR\Entity\EmployeeInterface;

interface EmployeeRepositoryInterface
{
    public function find(string $employeeId): EmployeeInterface;
}
