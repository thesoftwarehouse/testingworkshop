<?php

namespace App\HumanResources\Mailer;

interface MailerInterface
{
    public function send(string $email, string $message): void;
}
