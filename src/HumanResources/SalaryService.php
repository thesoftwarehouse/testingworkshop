<?php

namespace App\HumanResources;

use App\HumanResources\Configuration\ConfigurationProviderInterface;
use App\HumanResources\Entity\Salary;
use App\HumanResources\Exception\MailerException;
use App\HumanResources\Exception\SalaryException;
use App\HumanResources\Mailer\MailerInterface;
use App\HumanResources\Repository\EmployeeRepositoryInterface;
use App\HumanResources\Repository\SalaryRepositoryInterface;
use App\Money\Euro;

class SalaryService
{
    private $employeeRepository;
    private $salaryRepository;
    private $configurationProvider;
    private $mailer;

    public function __construct(
        EmployeeRepositoryInterface $employeeRepository,
        SalaryRepositoryInterface $salaryRepository,
        ConfigurationProviderInterface $configurationProvider,
        MailerInterface $mailer
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->salaryRepository = $salaryRepository;
        $this->configurationProvider = $configurationProvider;
        $this->mailer = $mailer;
    }

    public function createPaycheck(EmployeeId $employeeId, Euro $amount): void
    {
        try {
            $salary = new Salary($employeeId, $amount);
            $this->salaryRepository->save($salary);

            $hrEmail = $this->configurationProvider->get()->hrEmail();
            $employee = $this->employeeRepository->find($employeeId);

        } catch (\Exception $exception) {
            throw new SalaryException('Could not save salary');
        }

        try {
            $this->mailer->send($hrEmail, 'paycheck created');
            $this->mailer->send($employee->email(), 'paycheck created');
        } catch (\Exception $exception) {
            throw new MailerException('Could not send email');
        }
    }
}
