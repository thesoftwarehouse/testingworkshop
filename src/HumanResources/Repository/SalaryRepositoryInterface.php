<?php

namespace App\HumanResources\Repository;

use App\HumanResources\SalaryInterface;

interface SalaryRepositoryInterface
{
    public function save(SalaryInterface $salary): void;
}
