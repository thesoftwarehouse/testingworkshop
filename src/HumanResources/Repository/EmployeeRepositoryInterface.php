<?php

namespace App\HumanResources\Repository;

use App\HumanResources\EmployeeId;
use App\HumanResources\EmployeeInterface;

interface EmployeeRepositoryInterface
{
    public function find(EmployeeId $employeeId): EmployeeInterface;
}
