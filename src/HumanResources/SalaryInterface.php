<?php

namespace App\HumanResources;

use App\Money\Euro;

interface SalaryInterface
{
    public function employeeId(): EmployeeId;
    public function amount(): Euro;
    public function createdAt(): \DateTimeInterface;
}
