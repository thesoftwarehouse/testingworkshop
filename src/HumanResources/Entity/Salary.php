<?php

namespace App\HumanResources\Entity;

use App\HumanResources\EmployeeId;
use App\HumanResources\SalaryInterface;
use App\Money\Euro;

class Salary implements SalaryInterface
{
    private $employeeId;
    private $amount;
    private $createdAt;

    public function __construct(EmployeeId $employeeId, Euro $amount)
    {
        $this->employeeId = $employeeId;
        $this->amount = $amount;
        $this->createdAt = new \DateTimeImmutable();
    }

    public function employeeId(): EmployeeId
    {
        return $this->employeeId;
    }

    public function amount(): Euro
    {
        return $this->amount;
    }

    public function createdAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }
}
