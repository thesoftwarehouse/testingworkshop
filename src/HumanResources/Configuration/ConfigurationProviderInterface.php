<?php

namespace App\HumanResources\Configuration;

interface ConfigurationProviderInterface
{
    public function get(): Configuration;
}
