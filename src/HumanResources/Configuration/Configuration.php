<?php

namespace App\HumanResources\Configuration;

class Configuration
{
    private $hrEmail;

    public function __construct(string $hrEmail)
    {
        $this->hrEmail = $hrEmail;
    }

    public function hrEmail(): string
    {
        return $this->hrEmail;
    }
}
