<?php

namespace App\HumanResources;

interface EmployeeInterface
{
    public function id(): string;

    public function email(): string;
}
