<?php

namespace spec\App\HR;

use App\HR\Entity\EmployeeInterface;
use App\HR\Entity\SalaryInterface;
use App\HR\Mailer\MailerInterface;
use App\HR\Repository\EmployeeRepositoryInterface;
use App\HR\Repository\SalaryRepositoryInterface;
use App\Money\Euro;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class SalaryServiceSpec extends ObjectBehavior
{
    public function it_should_create_salary_for_employee(
        EmployeeInterface $employee,
        EmployeeRepositoryInterface $employeeRepository,
        SalaryRepositoryInterface $salaryRepository,
        MailerInterface $mailer
    ): void {
        $amount = new Euro(2100);
        $employeeId = 'jdoe-123';

        $employee->email()->willReturn('johndoe@foo.bar');
        $employeeRepository->find('jdoe-123')->willReturn($employee);

        $salaryRepository->save(Argument::type(SalaryInterface::class))->shouldBeCalled();

        $this->beConstructedWith(
            $employeeRepository,
            $salaryRepository,
            $mailer
        );

        $this->createPaycheck($employeeId, $amount);

        $mailer->send('johndoe@foo.bar', 'new salary')->shouldHaveBeenCalled();
        $mailer->send('hr@foo.bar', 'new salary')->shouldHaveBeenCalled();
    }
}
