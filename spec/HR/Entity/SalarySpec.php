<?php

namespace spec\App\HR\Entity;

use App\Money\Euro;
use PhpSpec\ObjectBehavior;

class SalarySpec extends ObjectBehavior
{
    public function it_should_return_employee_id(): void
    {
        $this->beConstructedWith('jdoe-456', new Euro(2100));
        $this->employeeId()->shouldReturn('jdoe-456');
    }

    public function it_should_return_amount(): void
    {
        $this->beConstructedWith('jdoe-456', new Euro(2200));
        $this->amount()->shouldBeLike(new Euro(2200));
    }

    public function it_should_return_creation_date(): void
    {
        $this->beConstructedWith('jdoe-456', new Euro(2200));
        $this->createdAt()->shouldBeAnInstanceOf(\DateTimeInterface::class);
    }
}
