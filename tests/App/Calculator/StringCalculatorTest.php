<?php
namespace Tests\App\Calculator;

use App\Calculator\Exception\InvalidInputStringException;
use App\Calculator\StringCalculator;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class StringCalculatorTest extends TestCase
{
    public function addDataProvider(): array
    {
        return [
            ['1,4,6,2', ',', 13],
            ['1;4;12;2', ';', 19],
            ['1|4', '|', 5],
            ['112+23', '+', 135],
        ];
    }

    public function multiplyDataProvider(): array
    {
        return [
            ['3,2,4', ',', 24],
            ['2;3;12;2', ';', 144],
            ['2|4', '|', 8],
            ['112+23', '+', 2576],
        ];
    }

    /**
     * @dataProvider addDataProvider
     */
    public function testAdd(string $testString, string $separator, int $testResult): void
    {
        $stringCalculator = new StringCalculator();
        $result = $stringCalculator->add($testString, $separator);

        Assert::assertEquals($testResult, $result);
    }

    public function testAddInvalidInput(): void
    {
        $this->expectException(InvalidInputStringException::class);
        $this->expectExceptionMessage("'1,2,invalid' is not valid input");

        $stringCalculator = new StringCalculator();
        $stringCalculator->add('1,2,invalid', ',');
    }

    /**
     * @dataProvider multiplyDataProvider
     */
    public function testMultiply(string $testString, string $separator, int $testResult): void
    {
        $stringCalculator = new StringCalculator();
        $result = $stringCalculator->multiply($testString, $separator);

        Assert::assertEquals($testResult, $result);
    }
}
