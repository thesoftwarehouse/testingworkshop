<?php

namespace Tests\App\Money;

use App\Money\Euro;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class EuroTest extends TestCase
{
    public function testMultiplication(): void
    {
        $seven = new Euro(7);
        Assert::assertEquals(new Euro(21), $seven->multiply(3));
        Assert::assertEquals(new Euro(14), $seven->multiply(2));
        Assert::assertEquals(new Euro(17.5), $seven->multiply(2.5));
    }

    public function testEquality(): void
    {
        $seven = new Euro(7);
        Assert::assertTrue($seven->equals(new Euro(7)));
        Assert::assertFalse($seven->equals(new Euro(6)));
        Assert::assertFalse($seven->equals(new Euro(7.01)));
    }
}
