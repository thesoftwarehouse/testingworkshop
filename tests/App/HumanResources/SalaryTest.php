<?php

namespace Tests\App\HumanResources;

use App\HumanResources\Configuration\Configuration;
use App\HumanResources\Configuration\ConfigurationProviderInterface;
use App\HumanResources\EmployeeId;
use App\HumanResources\EmployeeInterface;
use App\HumanResources\Exception\MailerException;
use App\HumanResources\Exception\SalaryException;
use App\HumanResources\Mailer\MailerInterface;
use App\HumanResources\Repository\EmployeeRepositoryInterface;
use App\HumanResources\Repository\SalaryRepositoryInterface;
use App\HumanResources\SalaryInterface;
use App\HumanResources\SalaryService;
use App\Money\Euro;
use Hamcrest\Core\IsInstanceOf;
use Hamcrest\Core\IsTypeOf;
use PHPUnit\Framework\TestCase;

class SalaryTest extends TestCase
{
    public function testAssignSalary(): void
    {
        $amount = new Euro(2100);
        $employeeId = new EmployeeId('123z');

        $employee = \Mockery::mock(EmployeeInterface::class);
        $employee->allows()->id()->andReturns('123z');
        $employee->allows()->email()->andReturns('foo@bar.com');

        // employee details should be fetched from a repository
        $employeeRepository = \Mockery::mock(EmployeeRepositoryInterface::class);
        $employeeRepository
            ->allows()
            ->find($employeeId)
            ->andReturns($employee);

        // salary must be persisted
        $salaryRepository = \Mockery::mock(SalaryRepositoryInterface::class);
        $salaryRepository
            ->expects()
            ->save(IsInstanceOf::anInstanceOf(SalaryInterface::class));

        // getting HR mail configuration
        $configurationProvider = \Mockery::mock(ConfigurationProviderInterface::class);
        $configurationProvider
            ->allows()
            ->get()
            ->andReturns(new Configuration('hr@bar.com'));

        // mailer
        $mailer = \Mockery::spy(MailerInterface::class);

        $salaryService = new SalaryService($employeeRepository, $salaryRepository, $configurationProvider, $mailer);
        $salaryService->createPaycheck($employeeId, $amount);

        $mailer->shouldHaveReceived('send')->twice();
    }

    public function testExceptionOnSavingFailure(): void
    {
        $amount = new Euro(2100);
        $employeeId = new EmployeeId('123z');

        // employee details should be fetched from a repository
        $employeeRepository = \Mockery::mock(EmployeeRepositoryInterface::class);
        $configurationProvider = \Mockery::mock(ConfigurationProviderInterface::class);
        $mailer = \Mockery::spy(MailerInterface::class);

        // salary must be persisted
        $salaryRepository = \Mockery::mock(SalaryRepositoryInterface::class);
        $salaryRepository
            ->expects()
            ->save(IsInstanceOf::anInstanceOf(SalaryInterface::class))
            ->andThrow(new \RuntimeException('Data'));


        $this->expectException(SalaryException::class);
        $this->expectExceptionMessage('Could not save salary');

        $salaryService = new SalaryService($employeeRepository, $salaryRepository, $configurationProvider, $mailer);
        $salaryService->createPaycheck($employeeId, $amount);
    }

    public function testExceptionOnNotificationFailure(): void
    {
        $amount = new Euro(2100);
        $employeeId = new EmployeeId('123z');

        $employee = \Mockery::mock(EmployeeInterface::class);
        $employee->allows()->id()->andReturns('123z');
        $employee->allows()->email()->andReturns('foo@bar.com');

        // employee details should be fetched from a repository
        $employeeRepository = \Mockery::mock(EmployeeRepositoryInterface::class);
        $employeeRepository
            ->allows()
            ->find($employeeId)
            ->andReturns($employee);

        // salary must be persisted
        $salaryRepository = \Mockery::mock(SalaryRepositoryInterface::class);
        $salaryRepository
            ->expects()
            ->save(IsInstanceOf::anInstanceOf(SalaryInterface::class));

        // getting HR mail configuration
        $configurationProvider = \Mockery::mock(ConfigurationProviderInterface::class);
        $configurationProvider
            ->allows()
            ->get()
            ->andReturns(new Configuration('hr@bar.com'));

        $mailer = \Mockery::mock(MailerInterface::class);
        $mailer
            ->allows()
            ->send(IsTypeOf::typeOf('string'),IsTypeOf::typeOf('string'))
            ->andThrow(new \Exception());

        $this->expectException(MailerException::class);
        $this->expectExceptionMessage('Could not send email');


        $salaryService = new SalaryService($employeeRepository, $salaryRepository, $configurationProvider, $mailer);
        $salaryService->createPaycheck($employeeId, $amount);
    }

    public function tearDown(): void
    {
        \Mockery::close();
    }
}
