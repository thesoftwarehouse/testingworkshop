const { BasePage } = require('kakunin');

class TodoPage extends BasePage {
  constructor() {
    super();

    this.url = '/examples/react/';

    this.header = $('h1');
    this.input = $('input');
    this.list = $('ul.todo-list');
    this.listItem = $$('ul.todo-list li');
    this.tutorialLink = $("a[href$='http://facebook.github.io/react/docs/tutorial.html']");
  }
}

module.exports = TodoPage;
