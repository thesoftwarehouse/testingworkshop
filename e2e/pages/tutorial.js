const { BasePage } = require('kakunin');

class TutorialPage extends BasePage {
  constructor() {
    super();

    this.url = 'https://reactjs.org/tutorial/tutorial.html';
  }
}

module.exports = TutorialPage;
