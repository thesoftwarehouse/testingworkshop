Feature:
    Scenario: ToDo list is displayed properly
        When I visit the "todo-list" page
        Then the "todo-list" page is displayed
        And I wait for "visibilityOf" of the "header" element
        And the "header" element is visible
        And there is element "header" containing "todos" text
        And the "input" element is visible

    Scenario: Adding new item to the list
        Given I visit the "todo-list" page
        When I fill the "input" form with:
            | input| new task |
        And I press the "enter" key
        Then the "list" element is visible
        And there is element "listItem" containing "new task" text
        And I fill the "input" form with:
            | input| second task |
        And I press the "enter" key
        Then there is element "listItem" containing "new task" text
        Then there is element "listItem" containing "second task" text

    Scenario: React tutorial page is reachable
        Given I visit the "todo-list" page
        When I click the "tutorialLink" element
        Then the "tutorial" page is displayed
